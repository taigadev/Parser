﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;

namespace ParsingSite
{
    class Program
    {
        static void Main()
        {
            List<IpAddress> list = new List<IpAddress>();
            try
            {
                var url = @"https://proxy.world/best";
                var web = new HtmlWeb();
                var htmlDoc = web.Load(url);
                Console.ForegroundColor = ConsoleColor.White;
                var nodes = htmlDoc.DocumentNode.SelectNodes("//div[@id='ipportproxydiv']");

                foreach (var item in nodes)
                {
                    int i1 = item.InnerHtml.IndexOf(">");
                    int i2 = item.InnerHtml.IndexOf("'>", i1 + 1);
                    string ip = item.InnerHtml.Substring(i1 + 1, item.InnerHtml.IndexOf("<", i1) - i1 - 1);
                    string port = item.InnerHtml.Substring(i2 + 2, item.InnerHtml.IndexOf("<", i2) - i2 - 2);
                    list.Add(new IpAddress(ip, port));
                    Console.WriteLine($"{ip}:{port} added!");
                }
                Console.WriteLine("--------------");
            }
            catch (Exception e) { Console.WriteLine($"first site is not working{e.Message}"); }
            try
            {
                var url1 = @"https://free-proxy-list.net/";
                var web1 = new HtmlWeb();
                var htmlDoc1 = web1.Load(url1);
                var nodes1 = htmlDoc1.DocumentNode.SelectNodes("//table/tbody/tr");

                foreach (var item in nodes1)
                {
                    int i1 = item.InnerHtml.IndexOf(">");
                    int i2 = item.InnerHtml.IndexOf("<td>", i1 + 1);
                    string ip = item.InnerHtml.Substring(i1 + 1, item.InnerHtml.IndexOf("<", i1) - i1 - 1);
                    string port = item.InnerHtml.Substring(i2 + 4, item.InnerHtml.IndexOf("</td>", i2) - i2 - 4);
                    list.Add(new IpAddress(ip, port));
                    Console.WriteLine($"{ip}:{port} added!");
                }
            }
            catch (Exception e) { Console.WriteLine($"first site is not working{e.Message}"); }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("starting respounce");

            foreach (var ipAddress in list)
            {
                string _url = @"http://steamcommunity.com/market/priceoverview/?appid=730&currency=5&market_hash_name=StatTrak%E2%84%A2%20M4A1-S%20%7C%20Hyper%20Beast%20(Minimal%20Wear)";
                _url = @"https://discordapp.com/hypesquad?ref=f9No8NRQ3A";
                _url = @"https://www.twitch.tv/jorro1";
                Task task = Task.Factory.StartNew(GetUrlRequestAsync, new Kostil(_url, ipAddress));
                currentCountOfTasks++;
                //Console.WriteLine(GetUrlRequest(_url, ipAddress));
            }
            Console.ForegroundColor = ConsoleColor.Blue;
            while (currentCountOfTasks > 0)
            {
                System.Threading.Thread.Sleep(15);
            }
            Console.ReadKey();
        }
        public static int currentCountOfTasks { get; set; } = 0;
        #region Not Usage
        //static string GetUrlRequest(string Url, IpAddress IP)
        //{
        //    string result = "";
        //    try
        //    {
        //        WebClient doc = new WebClient();
        //        WebProxy proxy = new WebProxy(IP.Ip, IP.Port);
        //        proxy.BypassProxyOnLocal = false;
        //        doc.Proxy = proxy;
        //        result = doc.DownloadString(new Uri(Url));
        //    }
        //    catch (Exception e)
        //    {
        //        result += $"Исключение: {e.Message}";
        //    }
        //    return result;
        //}
        #endregion
        static async Task<string> GetUrlRequestAsync(object obj) // string Url, IpAddress IP
        {
            Kostil kost = (Kostil)obj;
            WebClient doc = new WebClient();
            WebProxy proxy = new WebProxy(kost.ip.Ip, kost.ip.Port);
            proxy.BypassProxyOnLocal = false;
            doc.Proxy = proxy;
            string result = await doc.DownloadStringTaskAsync(new Uri(kost.url));
            //Console.Write($"{result} \n");
            Console.WriteLine($"Task finished! Еще {currentCountOfTasks}");
            currentCountOfTasks--;
            doc.Dispose(); // 19% decreased memory usage 72mb >> 58mb
            kost.Dispose();// 28% decreased memory usage 58mb >> 42mb
            return result;
        }
    }
    class Kostil : IDisposable
    {
        public IpAddress ip { get; set; }
        public string url { get; set; }
        public Kostil(string Url, IpAddress IP)
        {
            ip = IP;
            url = Url;
        }
        ~Kostil()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
    class IpAddress : IDisposable
    {
        public int Port { get; set; }
        public string Ip { get; set; }
        public IpAddress(object ip, object port)
        {
            try
            {
                Port = Convert.ToInt32(port);
            }
            catch
            {
                throw new Exception("Wrong port format (use int)");
            }
            Ip = ip.ToString();
        }
        public override string ToString()
        {
            return Ip.ToString() + Port.ToString();
        }
        ~IpAddress()
        {
            Dispose();
        }
        public void Dispose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
